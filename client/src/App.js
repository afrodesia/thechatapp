import React, { Fragment } from 'react'
import { BrowserRouter as Router, Switch, Link, Route } from 'react-router-dom'
import './App.scss';

import Join from './components/Join/'
import Chat from './components/Chat/'

function App() {
  return (
    <Fragment>
      <div className="App">
        <Router>
          <Route path="/" exact component={Join}/>
          <Route path="/chat" component={Chat}/>
        </Router>
      </div>
    </Fragment>
  );
}

export default App;
